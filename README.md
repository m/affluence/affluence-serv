# Affluence Serv

Backend Nodejs de l'application Affluence.

## Installation

`npm install`

## Exécution

* `npm run build` pour lancer la compilation (en mode production).
* `npm run start:debug` pour lancer le serveur en mode debug (rechargement/compilation à chaud + debugueur).

## REST API

| Ressource           | Méthode | Type | Documentation                |
| ------------------- | ------- | ---- | ---------------------------- |
| `/occupancy/report` | POST    | JSON | Enregistre une contribution. |
