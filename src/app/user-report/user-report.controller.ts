import { Context } from "koa";

import JoiErrors from "@app/joi-error.middleware";
import { Joi } from "koa-joi-router";
import { ClientFactoryService } from "@app/db/client.factory";
import { UserReportService } from "./user-report.service";
import RateLimitMiddleware from "@app/ratelimit.middleware";
import { AttachMiddleware, Controller, LoggerService, Post } from "@m-backend/core";

@AttachMiddleware([JoiErrors])
@Controller()
export default class UserReportController {
	constructor(
		private userReportService: UserReportService,
		private clientFactory: ClientFactoryService,
		private log: LoggerService
	) { }
	@AttachMiddleware([RateLimitMiddleware])
	@Post('/occupancy/report', {
		type: 'json',
		body: {
			time: Joi.date().timestamp().required(),
			stopId: Joi.string().required(),
			routeId: Joi.string().required(),
			directionId: Joi.number().valid(1, 2).required(),
			occupancy: Joi.string().valid('LOW', 'MIDDLE', 'HIGH').required(),
			headsign: Joi.string()
		}
	})
	async postUserReport(ctx: Context) {
		let client;
		try {
			var data = ctx.request.body;
			this.userReportService.addReport({
				time: data.time,
				stop_code: data.stopId,
				data: {
					route_code: data.routeId,
					direction_id: data.direction_id,
					occupancy: data.occupancy,
					headsign: data.headsign
				}
			})
			client = this.clientFactory.getAffluenceClient();
			ctx.response.body = { success: true, status: 200 };
		} catch (e: any)  {
			client?.end();
			this.log.logger.error(`[controller][UserReportController][postUserReport] ${e}`);
			ctx.response.body = { errors: [{ code: 'OPERATION_FAILED', message: e.message }] };
		}
	}
}
//TODO
//ecrire requetes quand on quitte ?
//verifier les stopId, routeId et leur coherence entre eux.
