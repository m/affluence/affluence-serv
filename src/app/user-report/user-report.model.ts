
export interface UserReport {
	time: number,
	stop_code: string,
	data: {
		route_code: string,
		direction_id: number,
		occupancy: string,
		headsign: string
	}
};
