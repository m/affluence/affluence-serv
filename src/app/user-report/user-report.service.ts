import { Service, LoggerService } from "@m-backend/core";
import { UserReport } from "./user-report.model";

@Service()
export class UserReportService {
	private _rawReports: UserReport[] = [];
	constructor(
		private log: LoggerService
	) { }
	addReport(report: UserReport) {
		this._rawReports.push(report);
	}
	getRawReportsAndFlush() {
		return this._rawReports.splice(0, this._rawReports.length);
	}
}
