import { Middleware, MiddlewareInterface, LoggerService } from "@m-backend/core";
import { Next, Context } from "koa";

@Middleware()
export default class JoiErrors implements MiddlewareInterface {

	constructor(private log: LoggerService) { }

	async use(ctx: Context, next: Next) {
		try {
			await next();
		} catch (e: any)  {
			if (e.isJoi) {
				this.log.logger.error(`[middleware][JoiErrors] ${e}`);
			}
			throw e;
		}
	}
}
