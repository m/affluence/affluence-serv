import MainApp from '@m-backend/core/src/lib/applications/main.app'
import { CoreModule, Module } from "@m-backend/core";
import UserReportController from './user-report/user-report.controller';
import DbCron from './db.cron';
import CorsMiddleware from './cors.middleware';
import JoiErrors from './joi-error.middleware';
import RateLimitMiddleware from './ratelimit.middleware';

@Module({
	imports: [CoreModule],
	applications: [MainApp],
	controllers: [UserReportController],
	crons: [DbCron],
	middlewares: [CorsMiddleware, RateLimitMiddleware, JoiErrors]
})
export class AppModule { }
