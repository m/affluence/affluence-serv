import { Context, Next } from 'koa';
import cors from '@koa/cors';
import { config, Middleware, MiddlewareInterface } from '@m-backend/core';



@Middleware({ autoloadIn: 'main' })
export default class CorsMiddleware implements MiddlewareInterface {
	async use(ctx: Context, next: Next) {
		return await cors({
			origin: config.app.externalUrl,
			allowMethods: 'GET,HEAD,PUT,POST,DELETE'
		})(ctx, next);
	}
}
