import { Middleware, MiddlewareInterface } from '@m-backend/core';
import { Context, Next } from 'koa';
import limit from 'koa-ratelimit';

const db = new Map();

@Middleware()
export default class RateLimitMiddleware implements MiddlewareInterface {

	async use(ctx: Context, next: Next) {
		return await limit({
			driver: 'memory',
			db: db,
			duration: 1000 * 60, //1 min
			max: 2,
			id: (ctx) => {
				let header;
				if (Array.isArray(ctx.request.header['x-real-ip'])) header = ctx.request.header['x-real-ip'][0];
				else header = ctx.request.header['x-real-ip'];

				return (header ? header : ctx.request.ip);
			},
			errorMessage: 'Qu\'est ce qu\'il nous fait Noureyef, là ?'
			//blackList: ['127.0.0.1']
		})(ctx, next);
	}
}
