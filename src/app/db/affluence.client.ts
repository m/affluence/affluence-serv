import { config, LoggerService } from "@m-backend/core";
import { Client, QueryConfig } from "pg";

export class AffluenceClient extends Client {

	constructor(private log: LoggerService) {
		super(config.db.affluence);
	}

	async connect(): Promise<void> {
		await super.connect();
		await this.query(`set search_path = '${config.db.affluence.schema}';`);
	}

	async insert(table: string, columns: string, values: string, params: Array<any>, name?: string): Promise<boolean> {
		try {
			let queryConfig: QueryConfig = {
				text: `insert into ${table} ${columns} values ${values};`,
				values: params
			}
			if (name) queryConfig.name = name;
			this.log.logger.silly(`[client][AffluenceClient][insert] ${queryConfig.text}`);
			let result = await this.query<{ ident: number }>(queryConfig);
			if (result.rowCount !== 1) throw new Error(`${table} : Unable to perform ${result.command}.`);
			return true;
		} catch (e: any)  {
			if (e.details) this.log.logger.error(`[client][AffluenceClient][insert] ${e.details}`);
			throw (e);
		}
	}
}
