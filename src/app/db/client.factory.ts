import { AffluenceClient } from "@app/db/affluence.client";
import { Service, LoggerService } from "@m-backend/core";

@Service()
export class ClientFactoryService {
	constructor(
		private log: LoggerService
	) { }
	getAffluenceClient(): AffluenceClient {
		return new AffluenceClient(this.log);
	}
}
