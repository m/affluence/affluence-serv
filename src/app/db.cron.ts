import { Cron, OnTick, OnInit, LoggerService, config } from "@m-backend/core";
import { ClientFactoryService } from "./db/client.factory";
import { UserReportService } from "./user-report/user-report.service";

@Cron({
	cronTime: '10 * * * * *',
	runOnInit: true,
	start: true,
	timeZone: 'Europe/Paris'
})
export default class DbCron implements OnTick, OnInit {

	constructor(
		private userReportService: UserReportService,
		private clientFactory: ClientFactoryService,
		private log: LoggerService
	) { }
	async onInit(): Promise<void> {
		let client;
		try {
			client = this.clientFactory.getAffluenceClient();
			await client.connect();
			await client.query(`CREATE TABLE IF NOT EXISTS ${config.db.affluence.schema}.user_report (stop_code text NOT NULL,time timestamp with time zone NOT NULL,data json, CONSTRAINT user_report_pk PRIMARY KEY(time,stop_code)) WITH (oids = false)`);
			client.end();

		} catch (e: any)  {
			client?.end();
			this.log.logger.error(`[cron][DbCron][onInit] ${e}`);
		}
	}
	async onTick(): Promise<void> {
		let client;
		try {
			let data = this.userReportService.getRawReportsAndFlush();
			client = this.clientFactory.getAffluenceClient();
			await client.connect();
			for (let report of data) {
				this.log.logger.debug(JSON.stringify(report));
				await client.insert('user_report', '(stop_code,time,data)', '($1,$2,$3)', [report.stop_code, report.time, JSON.stringify(report.data)]);
			}
			client.end();

		} catch (e: any)  {
			client?.end();
			this.log.logger.error(`[cron][DbCron][onTick] ${e}`);
		}
	}
}
