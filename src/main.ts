import 'reflect-metadata';
import { bootstrap, loadConfigFile } from '@m-backend/core';
import { AppModule } from '@app/app.module';

loadConfigFile('db');

bootstrap(AppModule);
