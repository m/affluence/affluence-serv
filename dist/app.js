/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppModule = void 0;
const main_app_1 = __importDefault(__webpack_require__(/*! @m-backend/core/src/lib/applications/main.app */ "@m-backend/core/src/lib/applications/main.app"));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
const user_report_controller_1 = __importDefault(__webpack_require__(/*! ./user-report/user-report.controller */ "./src/app/user-report/user-report.controller.ts"));
const db_cron_1 = __importDefault(__webpack_require__(/*! ./db.cron */ "./src/app/db.cron.ts"));
const cors_middleware_1 = __importDefault(__webpack_require__(/*! ./cors.middleware */ "./src/app/cors.middleware.ts"));
const joi_error_middleware_1 = __importDefault(__webpack_require__(/*! ./joi-error.middleware */ "./src/app/joi-error.middleware.ts"));
const ratelimit_middleware_1 = __importDefault(__webpack_require__(/*! ./ratelimit.middleware */ "./src/app/ratelimit.middleware.ts"));
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, core_1.Module)({
        imports: [core_1.CoreModule],
        applications: [main_app_1.default],
        controllers: [user_report_controller_1.default],
        crons: [db_cron_1.default],
        middlewares: [cors_middleware_1.default, ratelimit_middleware_1.default, joi_error_middleware_1.default]
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/cors.middleware.ts":
/*!************************************!*\
  !*** ./src/app/cors.middleware.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const cors_1 = __importDefault(__webpack_require__(/*! @koa/cors */ "@koa/cors"));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
let CorsMiddleware = class CorsMiddleware {
    async use(ctx, next) {
        return await (0, cors_1.default)({
            origin: core_1.config.app.externalUrl,
            allowMethods: 'GET,HEAD,PUT,POST,DELETE'
        })(ctx, next);
    }
};
CorsMiddleware = __decorate([
    (0, core_1.Middleware)({ autoloadIn: 'main' })
], CorsMiddleware);
exports["default"] = CorsMiddleware;


/***/ }),

/***/ "./src/app/db.cron.ts":
/*!****************************!*\
  !*** ./src/app/db.cron.ts ***!
  \****************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
const client_factory_1 = __webpack_require__(/*! ./db/client.factory */ "./src/app/db/client.factory.ts");
const user_report_service_1 = __webpack_require__(/*! ./user-report/user-report.service */ "./src/app/user-report/user-report.service.ts");
let DbCron = class DbCron {
    constructor(userReportService, clientFactory, log) {
        this.userReportService = userReportService;
        this.clientFactory = clientFactory;
        this.log = log;
    }
    async onInit() {
        let client;
        try {
            client = this.clientFactory.getAffluenceClient();
            await client.connect();
            await client.query(`CREATE TABLE IF NOT EXISTS ${core_1.config.db.affluence.schema}.user_report (stop_code text NOT NULL,time timestamp with time zone NOT NULL,data json, CONSTRAINT user_report_pk PRIMARY KEY(time,stop_code)) WITH (oids = false)`);
            client.end();
        }
        catch (e) {
            client === null || client === void 0 ? void 0 : client.end();
            this.log.logger.error(`[cron][DbCron][onInit] ${e}`);
        }
    }
    async onTick() {
        let client;
        try {
            let data = this.userReportService.getRawReportsAndFlush();
            client = this.clientFactory.getAffluenceClient();
            await client.connect();
            for (let report of data) {
                this.log.logger.debug(JSON.stringify(report));
                await client.insert('user_report', '(stop_code,time,data)', '($1,$2,$3)', [report.stop_code, report.time, JSON.stringify(report.data)]);
            }
            client.end();
        }
        catch (e) {
            client === null || client === void 0 ? void 0 : client.end();
            this.log.logger.error(`[cron][DbCron][onTick] ${e}`);
        }
    }
};
DbCron = __decorate([
    (0, core_1.Cron)({
        cronTime: '10 * * * * *',
        runOnInit: true,
        start: true,
        timeZone: 'Europe/Paris'
    }),
    __metadata("design:paramtypes", [user_report_service_1.UserReportService,
        client_factory_1.ClientFactoryService,
        core_1.LoggerService])
], DbCron);
exports["default"] = DbCron;


/***/ }),

/***/ "./src/app/db/affluence.client.ts":
/*!****************************************!*\
  !*** ./src/app/db/affluence.client.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AffluenceClient = void 0;
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
const pg_1 = __webpack_require__(/*! pg */ "pg");
class AffluenceClient extends pg_1.Client {
    constructor(log) {
        super(core_1.config.db.affluence);
        this.log = log;
    }
    async connect() {
        await super.connect();
        await this.query(`set search_path = '${core_1.config.db.affluence.schema}';`);
    }
    async insert(table, columns, values, params, name) {
        try {
            let queryConfig = {
                text: `insert into ${table} ${columns} values ${values};`,
                values: params
            };
            if (name)
                queryConfig.name = name;
            this.log.logger.silly(`[client][AffluenceClient][insert] ${queryConfig.text}`);
            let result = await this.query(queryConfig);
            if (result.rowCount !== 1)
                throw new Error(`${table} : Unable to perform ${result.command}.`);
            return true;
        }
        catch (e) {
            if (e.details)
                this.log.logger.error(`[client][AffluenceClient][insert] ${e.details}`);
            throw (e);
        }
    }
}
exports.AffluenceClient = AffluenceClient;


/***/ }),

/***/ "./src/app/db/client.factory.ts":
/*!**************************************!*\
  !*** ./src/app/db/client.factory.ts ***!
  \**************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ClientFactoryService = void 0;
const affluence_client_1 = __webpack_require__(/*! @app/db/affluence.client */ "./src/app/db/affluence.client.ts");
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
let ClientFactoryService = class ClientFactoryService {
    constructor(log) {
        this.log = log;
    }
    getAffluenceClient() {
        return new affluence_client_1.AffluenceClient(this.log);
    }
};
ClientFactoryService = __decorate([
    (0, core_1.Service)(),
    __metadata("design:paramtypes", [core_1.LoggerService])
], ClientFactoryService);
exports.ClientFactoryService = ClientFactoryService;


/***/ }),

/***/ "./src/app/joi-error.middleware.ts":
/*!*****************************************!*\
  !*** ./src/app/joi-error.middleware.ts ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
let JoiErrors = class JoiErrors {
    constructor(log) {
        this.log = log;
    }
    async use(ctx, next) {
        try {
            await next();
        }
        catch (e) {
            if (e.isJoi) {
                this.log.logger.error(`[middleware][JoiErrors] ${e}`);
            }
            throw e;
        }
    }
};
JoiErrors = __decorate([
    (0, core_1.Middleware)(),
    __metadata("design:paramtypes", [core_1.LoggerService])
], JoiErrors);
exports["default"] = JoiErrors;


/***/ }),

/***/ "./src/app/ratelimit.middleware.ts":
/*!*****************************************!*\
  !*** ./src/app/ratelimit.middleware.ts ***!
  \*****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
const koa_ratelimit_1 = __importDefault(__webpack_require__(/*! koa-ratelimit */ "koa-ratelimit"));
const db = new Map();
let RateLimitMiddleware = class RateLimitMiddleware {
    async use(ctx, next) {
        return await (0, koa_ratelimit_1.default)({
            driver: 'memory',
            db: db,
            duration: 1000 * 60,
            max: 2,
            id: (ctx) => {
                let header;
                if (Array.isArray(ctx.request.header['x-real-ip']))
                    header = ctx.request.header['x-real-ip'][0];
                else
                    header = ctx.request.header['x-real-ip'];
                return (header ? header : ctx.request.ip);
            },
            errorMessage: 'Qu\'est ce qu\'il nous fait Noureyef, là ?'
            //blackList: ['127.0.0.1']
        })(ctx, next);
    }
};
RateLimitMiddleware = __decorate([
    (0, core_1.Middleware)()
], RateLimitMiddleware);
exports["default"] = RateLimitMiddleware;


/***/ }),

/***/ "./src/app/user-report/user-report.controller.ts":
/*!*******************************************************!*\
  !*** ./src/app/user-report/user-report.controller.ts ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
const joi_error_middleware_1 = __importDefault(__webpack_require__(/*! @app/joi-error.middleware */ "./src/app/joi-error.middleware.ts"));
const koa_joi_router_1 = __webpack_require__(/*! koa-joi-router */ "koa-joi-router");
const client_factory_1 = __webpack_require__(/*! @app/db/client.factory */ "./src/app/db/client.factory.ts");
const user_report_service_1 = __webpack_require__(/*! ./user-report.service */ "./src/app/user-report/user-report.service.ts");
const ratelimit_middleware_1 = __importDefault(__webpack_require__(/*! @app/ratelimit.middleware */ "./src/app/ratelimit.middleware.ts"));
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
let UserReportController = class UserReportController {
    constructor(userReportService, clientFactory, log) {
        this.userReportService = userReportService;
        this.clientFactory = clientFactory;
        this.log = log;
    }
    async postUserReport(ctx) {
        let client;
        try {
            var data = ctx.request.body;
            this.userReportService.addReport({
                time: data.time,
                stop_code: data.stopId,
                data: {
                    route_code: data.routeId,
                    direction_id: data.direction_id,
                    occupancy: data.occupancy,
                    headsign: data.headsign
                }
            });
            client = this.clientFactory.getAffluenceClient();
            ctx.response.body = { success: true, status: 200 };
        }
        catch (e) {
            client === null || client === void 0 ? void 0 : client.end();
            this.log.logger.error(`[controller][UserReportController][postUserReport] ${e}`);
            ctx.response.body = { errors: [{ code: 'OPERATION_FAILED', message: e.message }] };
        }
    }
};
__decorate([
    (0, core_1.AttachMiddleware)([ratelimit_middleware_1.default]),
    (0, core_1.Post)('/occupancy/report', {
        type: 'json',
        body: {
            time: koa_joi_router_1.Joi.date().timestamp().required(),
            stopId: koa_joi_router_1.Joi.string().required(),
            routeId: koa_joi_router_1.Joi.string().required(),
            directionId: koa_joi_router_1.Joi.number().valid(1, 2).required(),
            occupancy: koa_joi_router_1.Joi.string().valid('LOW', 'MIDDLE', 'HIGH').required(),
            headsign: koa_joi_router_1.Joi.string()
        }
    }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserReportController.prototype, "postUserReport", null);
UserReportController = __decorate([
    (0, core_1.AttachMiddleware)([joi_error_middleware_1.default]),
    (0, core_1.Controller)(),
    __metadata("design:paramtypes", [user_report_service_1.UserReportService,
        client_factory_1.ClientFactoryService,
        core_1.LoggerService])
], UserReportController);
exports["default"] = UserReportController;
//TODO
//ecrire requetes quand on quitte ?
//verifier les stopId, routeId et leur coherence entre eux.


/***/ }),

/***/ "./src/app/user-report/user-report.service.ts":
/*!****************************************************!*\
  !*** ./src/app/user-report/user-report.service.ts ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {


var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.UserReportService = void 0;
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
let UserReportService = class UserReportService {
    constructor(log) {
        this.log = log;
        this._rawReports = [];
    }
    addReport(report) {
        this._rawReports.push(report);
    }
    getRawReportsAndFlush() {
        return this._rawReports.splice(0, this._rawReports.length);
    }
};
UserReportService = __decorate([
    (0, core_1.Service)(),
    __metadata("design:paramtypes", [core_1.LoggerService])
], UserReportService);
exports.UserReportService = UserReportService;


/***/ }),

/***/ "@koa/cors":
/*!****************************!*\
  !*** external "@koa/cors" ***!
  \****************************/
/***/ ((module) => {

module.exports = require("@koa/cors");

/***/ }),

/***/ "@m-backend/core":
/*!**********************************!*\
  !*** external "@m-backend/core" ***!
  \**********************************/
/***/ ((module) => {

module.exports = require("@m-backend/core");

/***/ }),

/***/ "@m-backend/core/src/lib/applications/main.app":
/*!****************************************************************!*\
  !*** external "@m-backend/core/src/lib/applications/main.app" ***!
  \****************************************************************/
/***/ ((module) => {

module.exports = require("@m-backend/core/src/lib/applications/main.app");

/***/ }),

/***/ "koa-joi-router":
/*!*********************************!*\
  !*** external "koa-joi-router" ***!
  \*********************************/
/***/ ((module) => {

module.exports = require("koa-joi-router");

/***/ }),

/***/ "koa-ratelimit":
/*!********************************!*\
  !*** external "koa-ratelimit" ***!
  \********************************/
/***/ ((module) => {

module.exports = require("koa-ratelimit");

/***/ }),

/***/ "pg":
/*!*********************!*\
  !*** external "pg" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("pg");

/***/ }),

/***/ "reflect-metadata":
/*!***********************************!*\
  !*** external "reflect-metadata" ***!
  \***********************************/
/***/ ((module) => {

module.exports = require("reflect-metadata");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
__webpack_require__(/*! reflect-metadata */ "reflect-metadata");
const core_1 = __webpack_require__(/*! @m-backend/core */ "@m-backend/core");
const app_module_1 = __webpack_require__(/*! @app/app.module */ "./src/app/app.module.ts");
(0, core_1.loadConfigFile)('db');
(0, core_1.bootstrap)(app_module_1.AppModule);

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSw4SkFBbUU7QUFDbkUsNkVBQXFEO0FBQ3JELHFLQUF3RTtBQUN4RSxnR0FBK0I7QUFDL0Isd0hBQStDO0FBQy9DLHVJQUErQztBQUMvQyx1SUFBeUQ7QUFTekQsSUFBYSxTQUFTLEdBQXRCLE1BQWEsU0FBUztDQUFJO0FBQWIsU0FBUztJQVByQixpQkFBTSxFQUFDO1FBQ1AsT0FBTyxFQUFFLENBQUMsaUJBQVUsQ0FBQztRQUNyQixZQUFZLEVBQUUsQ0FBQyxrQkFBTyxDQUFDO1FBQ3ZCLFdBQVcsRUFBRSxDQUFDLGdDQUFvQixDQUFDO1FBQ25DLEtBQUssRUFBRSxDQUFDLGlCQUFNLENBQUM7UUFDZixXQUFXLEVBQUUsQ0FBQyx5QkFBYyxFQUFFLDhCQUFtQixFQUFFLDhCQUFTLENBQUM7S0FDN0QsQ0FBQztHQUNXLFNBQVMsQ0FBSTtBQUFiLDhCQUFTOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZHRCLGtGQUE2QjtBQUM3Qiw2RUFBMEU7QUFLMUUsSUFBcUIsY0FBYyxHQUFuQyxNQUFxQixjQUFjO0lBQ2xDLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBWSxFQUFFLElBQVU7UUFDakMsT0FBTyxNQUFNLGtCQUFJLEVBQUM7WUFDakIsTUFBTSxFQUFFLGFBQU0sQ0FBQyxHQUFHLENBQUMsV0FBVztZQUM5QixZQUFZLEVBQUUsMEJBQTBCO1NBQ3hDLENBQUMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDZixDQUFDO0NBQ0Q7QUFQb0IsY0FBYztJQURsQyxxQkFBVSxFQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFDO0dBQ2QsY0FBYyxDQU9sQztxQkFQb0IsY0FBYzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1BuQyw2RUFBOEU7QUFDOUUsMEdBQTJEO0FBQzNELDJJQUFzRTtBQVF0RSxJQUFxQixNQUFNLEdBQTNCLE1BQXFCLE1BQU07SUFFMUIsWUFDUyxpQkFBb0MsRUFDcEMsYUFBbUMsRUFDbkMsR0FBa0I7UUFGbEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNwQyxrQkFBYSxHQUFiLGFBQWEsQ0FBc0I7UUFDbkMsUUFBRyxHQUFILEdBQUcsQ0FBZTtJQUN2QixDQUFDO0lBQ0wsS0FBSyxDQUFDLE1BQU07UUFDWCxJQUFJLE1BQU0sQ0FBQztRQUNYLElBQUk7WUFDSCxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQ2pELE1BQU0sTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQ3ZCLE1BQU0sTUFBTSxDQUFDLEtBQUssQ0FBQyw4QkFBOEIsYUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxvS0FBb0ssQ0FBQyxDQUFDO1lBQ2pQLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUViO1FBQUMsT0FBTyxDQUFNLEVBQUc7WUFDakIsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLEdBQUcsRUFBRSxDQUFDO1lBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3JEO0lBQ0YsQ0FBQztJQUNELEtBQUssQ0FBQyxNQUFNO1FBQ1gsSUFBSSxNQUFNLENBQUM7UUFDWCxJQUFJO1lBQ0gsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDMUQsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUNqRCxNQUFNLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUN2QixLQUFLLElBQUksTUFBTSxJQUFJLElBQUksRUFBRTtnQkFDeEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDOUMsTUFBTSxNQUFNLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSx1QkFBdUIsRUFBRSxZQUFZLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3hJO1lBQ0QsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBRWI7UUFBQyxPQUFPLENBQU0sRUFBRztZQUNqQixNQUFNLGFBQU4sTUFBTSx1QkFBTixNQUFNLENBQUUsR0FBRyxFQUFFLENBQUM7WUFDZCxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDckQ7SUFDRixDQUFDO0NBQ0Q7QUFyQ29CLE1BQU07SUFOMUIsZUFBSSxFQUFDO1FBQ0wsUUFBUSxFQUFFLGNBQWM7UUFDeEIsU0FBUyxFQUFFLElBQUk7UUFDZixLQUFLLEVBQUUsSUFBSTtRQUNYLFFBQVEsRUFBRSxjQUFjO0tBQ3hCLENBQUM7cUNBSTJCLHVDQUFpQjtRQUNyQixxQ0FBb0I7UUFDOUIsb0JBQWE7R0FMUCxNQUFNLENBcUMxQjtxQkFyQ29CLE1BQU07Ozs7Ozs7Ozs7Ozs7O0FDVjNCLDZFQUF3RDtBQUN4RCxpREFBeUM7QUFFekMsTUFBYSxlQUFnQixTQUFRLFdBQU07SUFFMUMsWUFBb0IsR0FBa0I7UUFDckMsS0FBSyxDQUFDLGFBQU0sQ0FBQyxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUM7UUFEUixRQUFHLEdBQUgsR0FBRyxDQUFlO0lBRXRDLENBQUM7SUFFRCxLQUFLLENBQUMsT0FBTztRQUNaLE1BQU0sS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3RCLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsYUFBTSxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFhLEVBQUUsT0FBZSxFQUFFLE1BQWMsRUFBRSxNQUFrQixFQUFFLElBQWE7UUFDN0YsSUFBSTtZQUNILElBQUksV0FBVyxHQUFnQjtnQkFDOUIsSUFBSSxFQUFFLGVBQWUsS0FBSyxJQUFJLE9BQU8sV0FBVyxNQUFNLEdBQUc7Z0JBQ3pELE1BQU0sRUFBRSxNQUFNO2FBQ2Q7WUFDRCxJQUFJLElBQUk7Z0JBQUUsV0FBVyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDbEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHFDQUFxQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUMvRSxJQUFJLE1BQU0sR0FBRyxNQUFNLElBQUksQ0FBQyxLQUFLLENBQW9CLFdBQVcsQ0FBQyxDQUFDO1lBQzlELElBQUksTUFBTSxDQUFDLFFBQVEsS0FBSyxDQUFDO2dCQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLHdCQUF3QixNQUFNLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUM5RixPQUFPLElBQUksQ0FBQztTQUNaO1FBQUMsT0FBTyxDQUFNLEVBQUc7WUFDakIsSUFBSSxDQUFDLENBQUMsT0FBTztnQkFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMscUNBQXFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZGLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNWO0lBQ0YsQ0FBQztDQUNEO0FBM0JELDBDQTJCQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QkQsbUhBQTJEO0FBQzNELDZFQUF5RDtBQUd6RCxJQUFhLG9CQUFvQixHQUFqQyxNQUFhLG9CQUFvQjtJQUNoQyxZQUNTLEdBQWtCO1FBQWxCLFFBQUcsR0FBSCxHQUFHLENBQWU7SUFDdkIsQ0FBQztJQUNMLGtCQUFrQjtRQUNqQixPQUFPLElBQUksa0NBQWUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEMsQ0FBQztDQUNEO0FBUFksb0JBQW9CO0lBRGhDLGtCQUFPLEdBQUU7cUNBR0ssb0JBQWE7R0FGZixvQkFBb0IsQ0FPaEM7QUFQWSxvREFBb0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKakMsNkVBQWlGO0FBSWpGLElBQXFCLFNBQVMsR0FBOUIsTUFBcUIsU0FBUztJQUU3QixZQUFvQixHQUFrQjtRQUFsQixRQUFHLEdBQUgsR0FBRyxDQUFlO0lBQUksQ0FBQztJQUUzQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQVksRUFBRSxJQUFVO1FBQ2pDLElBQUk7WUFDSCxNQUFNLElBQUksRUFBRSxDQUFDO1NBQ2I7UUFBQyxPQUFPLENBQU0sRUFBRztZQUNqQixJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLDJCQUEyQixDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3REO1lBQ0QsTUFBTSxDQUFDLENBQUM7U0FDUjtJQUNGLENBQUM7Q0FDRDtBQWRvQixTQUFTO0lBRDdCLHFCQUFVLEdBQUU7cUNBR2Esb0JBQWE7R0FGbEIsU0FBUyxDQWM3QjtxQkFkb0IsU0FBUzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0o5Qiw2RUFBa0U7QUFFbEUsbUdBQWtDO0FBRWxDLE1BQU0sRUFBRSxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7QUFHckIsSUFBcUIsbUJBQW1CLEdBQXhDLE1BQXFCLG1CQUFtQjtJQUV2QyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQVksRUFBRSxJQUFVO1FBQ2pDLE9BQU8sTUFBTSwyQkFBSyxFQUFDO1lBQ2xCLE1BQU0sRUFBRSxRQUFRO1lBQ2hCLEVBQUUsRUFBRSxFQUFFO1lBQ04sUUFBUSxFQUFFLElBQUksR0FBRyxFQUFFO1lBQ25CLEdBQUcsRUFBRSxDQUFDO1lBQ04sRUFBRSxFQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ1gsSUFBSSxNQUFNLENBQUM7Z0JBQ1gsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUFFLE1BQU0sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7b0JBQzNGLE1BQU0sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFFOUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUM7WUFDRCxZQUFZLEVBQUUsNENBQTRDO1lBQzFELDBCQUEwQjtTQUMxQixDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ2YsQ0FBQztDQUNEO0FBbkJvQixtQkFBbUI7SUFEdkMscUJBQVUsR0FBRTtHQUNRLG1CQUFtQixDQW1CdkM7cUJBbkJvQixtQkFBbUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMeEMsMElBQWtEO0FBQ2xELHFGQUFxQztBQUNyQyw2R0FBOEQ7QUFDOUQsK0hBQTBEO0FBQzFELDBJQUE0RDtBQUM1RCw2RUFBb0Y7QUFJcEYsSUFBcUIsb0JBQW9CLEdBQXpDLE1BQXFCLG9CQUFvQjtJQUN4QyxZQUNTLGlCQUFvQyxFQUNwQyxhQUFtQyxFQUNuQyxHQUFrQjtRQUZsQixzQkFBaUIsR0FBakIsaUJBQWlCLENBQW1CO1FBQ3BDLGtCQUFhLEdBQWIsYUFBYSxDQUFzQjtRQUNuQyxRQUFHLEdBQUgsR0FBRyxDQUFlO0lBQ3ZCLENBQUM7SUFhTCxLQUFLLENBQUMsY0FBYyxDQUFDLEdBQVk7UUFDaEMsSUFBSSxNQUFNLENBQUM7UUFDWCxJQUFJO1lBQ0gsSUFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7WUFDNUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQztnQkFDaEMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNmLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTTtnQkFDdEIsSUFBSSxFQUFFO29CQUNMLFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTztvQkFDeEIsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZO29CQUMvQixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7b0JBQ3pCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtpQkFDdkI7YUFDRCxDQUFDO1lBQ0YsTUFBTSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUNqRCxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksR0FBRyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDO1NBQ25EO1FBQUMsT0FBTyxDQUFNLEVBQUc7WUFDakIsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLEdBQUcsRUFBRSxDQUFDO1lBQ2QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLHNEQUFzRCxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pGLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUM7U0FDbkY7SUFDRixDQUFDO0NBQ0Q7QUF0QkE7SUFaQywyQkFBZ0IsRUFBQyxDQUFDLDhCQUFtQixDQUFDLENBQUM7SUFDdkMsZUFBSSxFQUFDLG1CQUFtQixFQUFFO1FBQzFCLElBQUksRUFBRSxNQUFNO1FBQ1osSUFBSSxFQUFFO1lBQ0wsSUFBSSxFQUFFLG9CQUFHLENBQUMsSUFBSSxFQUFFLENBQUMsU0FBUyxFQUFFLENBQUMsUUFBUSxFQUFFO1lBQ3ZDLE1BQU0sRUFBRSxvQkFBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLFFBQVEsRUFBRTtZQUMvQixPQUFPLEVBQUUsb0JBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLEVBQUU7WUFDaEMsV0FBVyxFQUFFLG9CQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7WUFDaEQsU0FBUyxFQUFFLG9CQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUMsUUFBUSxFQUFFO1lBQ2pFLFFBQVEsRUFBRSxvQkFBRyxDQUFDLE1BQU0sRUFBRTtTQUN0QjtLQUNELENBQUM7Ozs7MERBc0JEO0FBdkNtQixvQkFBb0I7SUFGeEMsMkJBQWdCLEVBQUMsQ0FBQyw4QkFBUyxDQUFDLENBQUM7SUFDN0IscUJBQVUsR0FBRTtxQ0FHZ0IsdUNBQWlCO1FBQ3JCLHFDQUFvQjtRQUM5QixvQkFBYTtHQUpQLG9CQUFvQixDQXdDeEM7cUJBeENvQixvQkFBb0I7QUF5Q3pDLE1BQU07QUFDTixtQ0FBbUM7QUFDbkMsMkRBQTJEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3REM0QsNkVBQXlEO0FBSXpELElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRTdCLFlBQ1MsR0FBa0I7UUFBbEIsUUFBRyxHQUFILEdBQUcsQ0FBZTtRQUZuQixnQkFBVyxHQUFpQixFQUFFLENBQUM7SUFHbkMsQ0FBQztJQUNMLFNBQVMsQ0FBQyxNQUFrQjtRQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBQ0QscUJBQXFCO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDNUQsQ0FBQztDQUNEO0FBWFksaUJBQWlCO0lBRDdCLGtCQUFPLEdBQUU7cUNBSUssb0JBQWE7R0FIZixpQkFBaUIsQ0FXN0I7QUFYWSw4Q0FBaUI7Ozs7Ozs7Ozs7O0FDSjlCOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7Ozs7O0FDQUE7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7OztBQ0FBOzs7Ozs7VUNBQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBO1VBQ0E7O1VBRUE7VUFDQTtVQUNBOzs7Ozs7Ozs7Ozs7QUN0QkEsZ0VBQTBCO0FBQzFCLDZFQUE0RDtBQUM1RCwyRkFBNEM7QUFFNUMseUJBQWMsRUFBQyxJQUFJLENBQUMsQ0FBQztBQUVyQixvQkFBUyxFQUFDLHNCQUFTLENBQUMsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2Ly4vc3JjL2FwcC9hcHAubW9kdWxlLnRzIiwid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2Ly4vc3JjL2FwcC9jb3JzLm1pZGRsZXdhcmUudHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvLi9zcmMvYXBwL2RiLmNyb24udHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvLi9zcmMvYXBwL2RiL2FmZmx1ZW5jZS5jbGllbnQudHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvLi9zcmMvYXBwL2RiL2NsaWVudC5mYWN0b3J5LnRzIiwid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2Ly4vc3JjL2FwcC9qb2ktZXJyb3IubWlkZGxld2FyZS50cyIsIndlYnBhY2s6Ly9hZmZsdWVuY2Utc2Vydi8uL3NyYy9hcHAvcmF0ZWxpbWl0Lm1pZGRsZXdhcmUudHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvLi9zcmMvYXBwL3VzZXItcmVwb3J0L3VzZXItcmVwb3J0LmNvbnRyb2xsZXIudHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvLi9zcmMvYXBwL3VzZXItcmVwb3J0L3VzZXItcmVwb3J0LnNlcnZpY2UudHMiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvZXh0ZXJuYWwgY29tbW9uanMgXCJAa29hL2NvcnNcIiIsIndlYnBhY2s6Ly9hZmZsdWVuY2Utc2Vydi9leHRlcm5hbCBjb21tb25qcyBcIkBtLWJhY2tlbmQvY29yZVwiIiwid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2L2V4dGVybmFsIGNvbW1vbmpzIFwiQG0tYmFja2VuZC9jb3JlL3NyYy9saWIvYXBwbGljYXRpb25zL21haW4uYXBwXCIiLCJ3ZWJwYWNrOi8vYWZmbHVlbmNlLXNlcnYvZXh0ZXJuYWwgY29tbW9uanMgXCJrb2Etam9pLXJvdXRlclwiIiwid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2L2V4dGVybmFsIGNvbW1vbmpzIFwia29hLXJhdGVsaW1pdFwiIiwid2VicGFjazovL2FmZmx1ZW5jZS1zZXJ2L2V4dGVybmFsIGNvbW1vbmpzIFwicGdcIiIsIndlYnBhY2s6Ly9hZmZsdWVuY2Utc2Vydi9leHRlcm5hbCBjb21tb25qcyBcInJlZmxlY3QtbWV0YWRhdGFcIiIsIndlYnBhY2s6Ly9hZmZsdWVuY2Utc2Vydi93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9hZmZsdWVuY2Utc2Vydi8uL3NyYy9tYWluLnRzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBNYWluQXBwIGZyb20gJ0BtLWJhY2tlbmQvY29yZS9zcmMvbGliL2FwcGxpY2F0aW9ucy9tYWluLmFwcCdcbmltcG9ydCB7IENvcmVNb2R1bGUsIE1vZHVsZSB9IGZyb20gXCJAbS1iYWNrZW5kL2NvcmVcIjtcbmltcG9ydCBVc2VyUmVwb3J0Q29udHJvbGxlciBmcm9tICcuL3VzZXItcmVwb3J0L3VzZXItcmVwb3J0LmNvbnRyb2xsZXInO1xuaW1wb3J0IERiQ3JvbiBmcm9tICcuL2RiLmNyb24nO1xuaW1wb3J0IENvcnNNaWRkbGV3YXJlIGZyb20gJy4vY29ycy5taWRkbGV3YXJlJztcbmltcG9ydCBKb2lFcnJvcnMgZnJvbSAnLi9qb2ktZXJyb3IubWlkZGxld2FyZSc7XG5pbXBvcnQgUmF0ZUxpbWl0TWlkZGxld2FyZSBmcm9tICcuL3JhdGVsaW1pdC5taWRkbGV3YXJlJztcblxuQE1vZHVsZSh7XG5cdGltcG9ydHM6IFtDb3JlTW9kdWxlXSxcblx0YXBwbGljYXRpb25zOiBbTWFpbkFwcF0sXG5cdGNvbnRyb2xsZXJzOiBbVXNlclJlcG9ydENvbnRyb2xsZXJdLFxuXHRjcm9uczogW0RiQ3Jvbl0sXG5cdG1pZGRsZXdhcmVzOiBbQ29yc01pZGRsZXdhcmUsIFJhdGVMaW1pdE1pZGRsZXdhcmUsIEpvaUVycm9yc11cbn0pXG5leHBvcnQgY2xhc3MgQXBwTW9kdWxlIHsgfVxuIiwiaW1wb3J0IHsgQ29udGV4dCwgTmV4dCB9IGZyb20gJ2tvYSc7XG5pbXBvcnQgY29ycyBmcm9tICdAa29hL2NvcnMnO1xuaW1wb3J0IHsgY29uZmlnLCBNaWRkbGV3YXJlLCBNaWRkbGV3YXJlSW50ZXJmYWNlIH0gZnJvbSAnQG0tYmFja2VuZC9jb3JlJztcblxuXG5cbkBNaWRkbGV3YXJlKHsgYXV0b2xvYWRJbjogJ21haW4nIH0pXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDb3JzTWlkZGxld2FyZSBpbXBsZW1lbnRzIE1pZGRsZXdhcmVJbnRlcmZhY2Uge1xuXHRhc3luYyB1c2UoY3R4OiBDb250ZXh0LCBuZXh0OiBOZXh0KSB7XG5cdFx0cmV0dXJuIGF3YWl0IGNvcnMoe1xuXHRcdFx0b3JpZ2luOiBjb25maWcuYXBwLmV4dGVybmFsVXJsLFxuXHRcdFx0YWxsb3dNZXRob2RzOiAnR0VULEhFQUQsUFVULFBPU1QsREVMRVRFJ1xuXHRcdH0pKGN0eCwgbmV4dCk7XG5cdH1cbn1cbiIsImltcG9ydCB7IENyb24sIE9uVGljaywgT25Jbml0LCBMb2dnZXJTZXJ2aWNlLCBjb25maWcgfSBmcm9tIFwiQG0tYmFja2VuZC9jb3JlXCI7XG5pbXBvcnQgeyBDbGllbnRGYWN0b3J5U2VydmljZSB9IGZyb20gXCIuL2RiL2NsaWVudC5mYWN0b3J5XCI7XG5pbXBvcnQgeyBVc2VyUmVwb3J0U2VydmljZSB9IGZyb20gXCIuL3VzZXItcmVwb3J0L3VzZXItcmVwb3J0LnNlcnZpY2VcIjtcblxuQENyb24oe1xuXHRjcm9uVGltZTogJzEwICogKiAqICogKicsXG5cdHJ1bk9uSW5pdDogdHJ1ZSxcblx0c3RhcnQ6IHRydWUsXG5cdHRpbWVab25lOiAnRXVyb3BlL1BhcmlzJ1xufSlcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERiQ3JvbiBpbXBsZW1lbnRzIE9uVGljaywgT25Jbml0IHtcblxuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHVzZXJSZXBvcnRTZXJ2aWNlOiBVc2VyUmVwb3J0U2VydmljZSxcblx0XHRwcml2YXRlIGNsaWVudEZhY3Rvcnk6IENsaWVudEZhY3RvcnlTZXJ2aWNlLFxuXHRcdHByaXZhdGUgbG9nOiBMb2dnZXJTZXJ2aWNlXG5cdCkgeyB9XG5cdGFzeW5jIG9uSW5pdCgpOiBQcm9taXNlPHZvaWQ+IHtcblx0XHRsZXQgY2xpZW50O1xuXHRcdHRyeSB7XG5cdFx0XHRjbGllbnQgPSB0aGlzLmNsaWVudEZhY3RvcnkuZ2V0QWZmbHVlbmNlQ2xpZW50KCk7XG5cdFx0XHRhd2FpdCBjbGllbnQuY29ubmVjdCgpO1xuXHRcdFx0YXdhaXQgY2xpZW50LnF1ZXJ5KGBDUkVBVEUgVEFCTEUgSUYgTk9UIEVYSVNUUyAke2NvbmZpZy5kYi5hZmZsdWVuY2Uuc2NoZW1hfS51c2VyX3JlcG9ydCAoc3RvcF9jb2RlIHRleHQgTk9UIE5VTEwsdGltZSB0aW1lc3RhbXAgd2l0aCB0aW1lIHpvbmUgTk9UIE5VTEwsZGF0YSBqc29uLCBDT05TVFJBSU5UIHVzZXJfcmVwb3J0X3BrIFBSSU1BUlkgS0VZKHRpbWUsc3RvcF9jb2RlKSkgV0lUSCAob2lkcyA9IGZhbHNlKWApO1xuXHRcdFx0Y2xpZW50LmVuZCgpO1xuXG5cdFx0fSBjYXRjaCAoZTogYW55KSAge1xuXHRcdFx0Y2xpZW50Py5lbmQoKTtcblx0XHRcdHRoaXMubG9nLmxvZ2dlci5lcnJvcihgW2Nyb25dW0RiQ3Jvbl1bb25Jbml0XSAke2V9YCk7XG5cdFx0fVxuXHR9XG5cdGFzeW5jIG9uVGljaygpOiBQcm9taXNlPHZvaWQ+IHtcblx0XHRsZXQgY2xpZW50O1xuXHRcdHRyeSB7XG5cdFx0XHRsZXQgZGF0YSA9IHRoaXMudXNlclJlcG9ydFNlcnZpY2UuZ2V0UmF3UmVwb3J0c0FuZEZsdXNoKCk7XG5cdFx0XHRjbGllbnQgPSB0aGlzLmNsaWVudEZhY3RvcnkuZ2V0QWZmbHVlbmNlQ2xpZW50KCk7XG5cdFx0XHRhd2FpdCBjbGllbnQuY29ubmVjdCgpO1xuXHRcdFx0Zm9yIChsZXQgcmVwb3J0IG9mIGRhdGEpIHtcblx0XHRcdFx0dGhpcy5sb2cubG9nZ2VyLmRlYnVnKEpTT04uc3RyaW5naWZ5KHJlcG9ydCkpO1xuXHRcdFx0XHRhd2FpdCBjbGllbnQuaW5zZXJ0KCd1c2VyX3JlcG9ydCcsICcoc3RvcF9jb2RlLHRpbWUsZGF0YSknLCAnKCQxLCQyLCQzKScsIFtyZXBvcnQuc3RvcF9jb2RlLCByZXBvcnQudGltZSwgSlNPTi5zdHJpbmdpZnkocmVwb3J0LmRhdGEpXSk7XG5cdFx0XHR9XG5cdFx0XHRjbGllbnQuZW5kKCk7XG5cblx0XHR9IGNhdGNoIChlOiBhbnkpICB7XG5cdFx0XHRjbGllbnQ/LmVuZCgpO1xuXHRcdFx0dGhpcy5sb2cubG9nZ2VyLmVycm9yKGBbY3Jvbl1bRGJDcm9uXVtvblRpY2tdICR7ZX1gKTtcblx0XHR9XG5cdH1cbn1cbiIsImltcG9ydCB7IGNvbmZpZywgTG9nZ2VyU2VydmljZSB9IGZyb20gXCJAbS1iYWNrZW5kL2NvcmVcIjtcbmltcG9ydCB7IENsaWVudCwgUXVlcnlDb25maWcgfSBmcm9tIFwicGdcIjtcblxuZXhwb3J0IGNsYXNzIEFmZmx1ZW5jZUNsaWVudCBleHRlbmRzIENsaWVudCB7XG5cblx0Y29uc3RydWN0b3IocHJpdmF0ZSBsb2c6IExvZ2dlclNlcnZpY2UpIHtcblx0XHRzdXBlcihjb25maWcuZGIuYWZmbHVlbmNlKTtcblx0fVxuXG5cdGFzeW5jIGNvbm5lY3QoKTogUHJvbWlzZTx2b2lkPiB7XG5cdFx0YXdhaXQgc3VwZXIuY29ubmVjdCgpO1xuXHRcdGF3YWl0IHRoaXMucXVlcnkoYHNldCBzZWFyY2hfcGF0aCA9ICcke2NvbmZpZy5kYi5hZmZsdWVuY2Uuc2NoZW1hfSc7YCk7XG5cdH1cblxuXHRhc3luYyBpbnNlcnQodGFibGU6IHN0cmluZywgY29sdW1uczogc3RyaW5nLCB2YWx1ZXM6IHN0cmluZywgcGFyYW1zOiBBcnJheTxhbnk+LCBuYW1lPzogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XG5cdFx0dHJ5IHtcblx0XHRcdGxldCBxdWVyeUNvbmZpZzogUXVlcnlDb25maWcgPSB7XG5cdFx0XHRcdHRleHQ6IGBpbnNlcnQgaW50byAke3RhYmxlfSAke2NvbHVtbnN9IHZhbHVlcyAke3ZhbHVlc307YCxcblx0XHRcdFx0dmFsdWVzOiBwYXJhbXNcblx0XHRcdH1cblx0XHRcdGlmIChuYW1lKSBxdWVyeUNvbmZpZy5uYW1lID0gbmFtZTtcblx0XHRcdHRoaXMubG9nLmxvZ2dlci5zaWxseShgW2NsaWVudF1bQWZmbHVlbmNlQ2xpZW50XVtpbnNlcnRdICR7cXVlcnlDb25maWcudGV4dH1gKTtcblx0XHRcdGxldCByZXN1bHQgPSBhd2FpdCB0aGlzLnF1ZXJ5PHsgaWRlbnQ6IG51bWJlciB9PihxdWVyeUNvbmZpZyk7XG5cdFx0XHRpZiAocmVzdWx0LnJvd0NvdW50ICE9PSAxKSB0aHJvdyBuZXcgRXJyb3IoYCR7dGFibGV9IDogVW5hYmxlIHRvIHBlcmZvcm0gJHtyZXN1bHQuY29tbWFuZH0uYCk7XG5cdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHR9IGNhdGNoIChlOiBhbnkpICB7XG5cdFx0XHRpZiAoZS5kZXRhaWxzKSB0aGlzLmxvZy5sb2dnZXIuZXJyb3IoYFtjbGllbnRdW0FmZmx1ZW5jZUNsaWVudF1baW5zZXJ0XSAke2UuZGV0YWlsc31gKTtcblx0XHRcdHRocm93IChlKTtcblx0XHR9XG5cdH1cbn1cbiIsImltcG9ydCB7IEFmZmx1ZW5jZUNsaWVudCB9IGZyb20gXCJAYXBwL2RiL2FmZmx1ZW5jZS5jbGllbnRcIjtcbmltcG9ydCB7IFNlcnZpY2UsIExvZ2dlclNlcnZpY2UgfSBmcm9tIFwiQG0tYmFja2VuZC9jb3JlXCI7XG5cbkBTZXJ2aWNlKClcbmV4cG9ydCBjbGFzcyBDbGllbnRGYWN0b3J5U2VydmljZSB7XG5cdGNvbnN0cnVjdG9yKFxuXHRcdHByaXZhdGUgbG9nOiBMb2dnZXJTZXJ2aWNlXG5cdCkgeyB9XG5cdGdldEFmZmx1ZW5jZUNsaWVudCgpOiBBZmZsdWVuY2VDbGllbnQge1xuXHRcdHJldHVybiBuZXcgQWZmbHVlbmNlQ2xpZW50KHRoaXMubG9nKTtcblx0fVxufVxuIiwiaW1wb3J0IHsgTWlkZGxld2FyZSwgTWlkZGxld2FyZUludGVyZmFjZSwgTG9nZ2VyU2VydmljZSB9IGZyb20gXCJAbS1iYWNrZW5kL2NvcmVcIjtcbmltcG9ydCB7IE5leHQsIENvbnRleHQgfSBmcm9tIFwia29hXCI7XG5cbkBNaWRkbGV3YXJlKClcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEpvaUVycm9ycyBpbXBsZW1lbnRzIE1pZGRsZXdhcmVJbnRlcmZhY2Uge1xuXG5cdGNvbnN0cnVjdG9yKHByaXZhdGUgbG9nOiBMb2dnZXJTZXJ2aWNlKSB7IH1cblxuXHRhc3luYyB1c2UoY3R4OiBDb250ZXh0LCBuZXh0OiBOZXh0KSB7XG5cdFx0dHJ5IHtcblx0XHRcdGF3YWl0IG5leHQoKTtcblx0XHR9IGNhdGNoIChlOiBhbnkpICB7XG5cdFx0XHRpZiAoZS5pc0pvaSkge1xuXHRcdFx0XHR0aGlzLmxvZy5sb2dnZXIuZXJyb3IoYFttaWRkbGV3YXJlXVtKb2lFcnJvcnNdICR7ZX1gKTtcblx0XHRcdH1cblx0XHRcdHRocm93IGU7XG5cdFx0fVxuXHR9XG59XG4iLCJpbXBvcnQgeyBNaWRkbGV3YXJlLCBNaWRkbGV3YXJlSW50ZXJmYWNlIH0gZnJvbSAnQG0tYmFja2VuZC9jb3JlJztcbmltcG9ydCB7IENvbnRleHQsIE5leHQgfSBmcm9tICdrb2EnO1xuaW1wb3J0IGxpbWl0IGZyb20gJ2tvYS1yYXRlbGltaXQnO1xuXG5jb25zdCBkYiA9IG5ldyBNYXAoKTtcblxuQE1pZGRsZXdhcmUoKVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUmF0ZUxpbWl0TWlkZGxld2FyZSBpbXBsZW1lbnRzIE1pZGRsZXdhcmVJbnRlcmZhY2Uge1xuXG5cdGFzeW5jIHVzZShjdHg6IENvbnRleHQsIG5leHQ6IE5leHQpIHtcblx0XHRyZXR1cm4gYXdhaXQgbGltaXQoe1xuXHRcdFx0ZHJpdmVyOiAnbWVtb3J5Jyxcblx0XHRcdGRiOiBkYixcblx0XHRcdGR1cmF0aW9uOiAxMDAwICogNjAsIC8vMSBtaW5cblx0XHRcdG1heDogMixcblx0XHRcdGlkOiAoY3R4KSA9PiB7XG5cdFx0XHRcdGxldCBoZWFkZXI7XG5cdFx0XHRcdGlmIChBcnJheS5pc0FycmF5KGN0eC5yZXF1ZXN0LmhlYWRlclsneC1yZWFsLWlwJ10pKSBoZWFkZXIgPSBjdHgucmVxdWVzdC5oZWFkZXJbJ3gtcmVhbC1pcCddWzBdO1xuXHRcdFx0XHRlbHNlIGhlYWRlciA9IGN0eC5yZXF1ZXN0LmhlYWRlclsneC1yZWFsLWlwJ107XG5cblx0XHRcdFx0cmV0dXJuIChoZWFkZXIgPyBoZWFkZXIgOiBjdHgucmVxdWVzdC5pcCk7XG5cdFx0XHR9LFxuXHRcdFx0ZXJyb3JNZXNzYWdlOiAnUXVcXCdlc3QgY2UgcXVcXCdpbCBub3VzIGZhaXQgTm91cmV5ZWYsIGzDoCA/J1xuXHRcdFx0Ly9ibGFja0xpc3Q6IFsnMTI3LjAuMC4xJ11cblx0XHR9KShjdHgsIG5leHQpO1xuXHR9XG59XG4iLCJpbXBvcnQgeyBDb250ZXh0IH0gZnJvbSBcImtvYVwiO1xuXG5pbXBvcnQgSm9pRXJyb3JzIGZyb20gXCJAYXBwL2pvaS1lcnJvci5taWRkbGV3YXJlXCI7XG5pbXBvcnQgeyBKb2kgfSBmcm9tIFwia29hLWpvaS1yb3V0ZXJcIjtcbmltcG9ydCB7IENsaWVudEZhY3RvcnlTZXJ2aWNlIH0gZnJvbSBcIkBhcHAvZGIvY2xpZW50LmZhY3RvcnlcIjtcbmltcG9ydCB7IFVzZXJSZXBvcnRTZXJ2aWNlIH0gZnJvbSBcIi4vdXNlci1yZXBvcnQuc2VydmljZVwiO1xuaW1wb3J0IFJhdGVMaW1pdE1pZGRsZXdhcmUgZnJvbSBcIkBhcHAvcmF0ZWxpbWl0Lm1pZGRsZXdhcmVcIjtcbmltcG9ydCB7IEF0dGFjaE1pZGRsZXdhcmUsIENvbnRyb2xsZXIsIExvZ2dlclNlcnZpY2UsIFBvc3QgfSBmcm9tIFwiQG0tYmFja2VuZC9jb3JlXCI7XG5cbkBBdHRhY2hNaWRkbGV3YXJlKFtKb2lFcnJvcnNdKVxuQENvbnRyb2xsZXIoKVxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVXNlclJlcG9ydENvbnRyb2xsZXIge1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIHVzZXJSZXBvcnRTZXJ2aWNlOiBVc2VyUmVwb3J0U2VydmljZSxcblx0XHRwcml2YXRlIGNsaWVudEZhY3Rvcnk6IENsaWVudEZhY3RvcnlTZXJ2aWNlLFxuXHRcdHByaXZhdGUgbG9nOiBMb2dnZXJTZXJ2aWNlXG5cdCkgeyB9XG5cdEBBdHRhY2hNaWRkbGV3YXJlKFtSYXRlTGltaXRNaWRkbGV3YXJlXSlcblx0QFBvc3QoJy9vY2N1cGFuY3kvcmVwb3J0Jywge1xuXHRcdHR5cGU6ICdqc29uJyxcblx0XHRib2R5OiB7XG5cdFx0XHR0aW1lOiBKb2kuZGF0ZSgpLnRpbWVzdGFtcCgpLnJlcXVpcmVkKCksXG5cdFx0XHRzdG9wSWQ6IEpvaS5zdHJpbmcoKS5yZXF1aXJlZCgpLFxuXHRcdFx0cm91dGVJZDogSm9pLnN0cmluZygpLnJlcXVpcmVkKCksXG5cdFx0XHRkaXJlY3Rpb25JZDogSm9pLm51bWJlcigpLnZhbGlkKDEsIDIpLnJlcXVpcmVkKCksXG5cdFx0XHRvY2N1cGFuY3k6IEpvaS5zdHJpbmcoKS52YWxpZCgnTE9XJywgJ01JRERMRScsICdISUdIJykucmVxdWlyZWQoKSxcblx0XHRcdGhlYWRzaWduOiBKb2kuc3RyaW5nKClcblx0XHR9XG5cdH0pXG5cdGFzeW5jIHBvc3RVc2VyUmVwb3J0KGN0eDogQ29udGV4dCkge1xuXHRcdGxldCBjbGllbnQ7XG5cdFx0dHJ5IHtcblx0XHRcdHZhciBkYXRhID0gY3R4LnJlcXVlc3QuYm9keTtcblx0XHRcdHRoaXMudXNlclJlcG9ydFNlcnZpY2UuYWRkUmVwb3J0KHtcblx0XHRcdFx0dGltZTogZGF0YS50aW1lLFxuXHRcdFx0XHRzdG9wX2NvZGU6IGRhdGEuc3RvcElkLFxuXHRcdFx0XHRkYXRhOiB7XG5cdFx0XHRcdFx0cm91dGVfY29kZTogZGF0YS5yb3V0ZUlkLFxuXHRcdFx0XHRcdGRpcmVjdGlvbl9pZDogZGF0YS5kaXJlY3Rpb25faWQsXG5cdFx0XHRcdFx0b2NjdXBhbmN5OiBkYXRhLm9jY3VwYW5jeSxcblx0XHRcdFx0XHRoZWFkc2lnbjogZGF0YS5oZWFkc2lnblxuXHRcdFx0XHR9XG5cdFx0XHR9KVxuXHRcdFx0Y2xpZW50ID0gdGhpcy5jbGllbnRGYWN0b3J5LmdldEFmZmx1ZW5jZUNsaWVudCgpO1xuXHRcdFx0Y3R4LnJlc3BvbnNlLmJvZHkgPSB7IHN1Y2Nlc3M6IHRydWUsIHN0YXR1czogMjAwIH07XG5cdFx0fSBjYXRjaCAoZTogYW55KSAge1xuXHRcdFx0Y2xpZW50Py5lbmQoKTtcblx0XHRcdHRoaXMubG9nLmxvZ2dlci5lcnJvcihgW2NvbnRyb2xsZXJdW1VzZXJSZXBvcnRDb250cm9sbGVyXVtwb3N0VXNlclJlcG9ydF0gJHtlfWApO1xuXHRcdFx0Y3R4LnJlc3BvbnNlLmJvZHkgPSB7IGVycm9yczogW3sgY29kZTogJ09QRVJBVElPTl9GQUlMRUQnLCBtZXNzYWdlOiBlLm1lc3NhZ2UgfV0gfTtcblx0XHR9XG5cdH1cbn1cbi8vVE9ET1xuLy9lY3JpcmUgcmVxdWV0ZXMgcXVhbmQgb24gcXVpdHRlID9cbi8vdmVyaWZpZXIgbGVzIHN0b3BJZCwgcm91dGVJZCBldCBsZXVyIGNvaGVyZW5jZSBlbnRyZSBldXguXG4iLCJpbXBvcnQgeyBTZXJ2aWNlLCBMb2dnZXJTZXJ2aWNlIH0gZnJvbSBcIkBtLWJhY2tlbmQvY29yZVwiO1xuaW1wb3J0IHsgVXNlclJlcG9ydCB9IGZyb20gXCIuL3VzZXItcmVwb3J0Lm1vZGVsXCI7XG5cbkBTZXJ2aWNlKClcbmV4cG9ydCBjbGFzcyBVc2VyUmVwb3J0U2VydmljZSB7XG5cdHByaXZhdGUgX3Jhd1JlcG9ydHM6IFVzZXJSZXBvcnRbXSA9IFtdO1xuXHRjb25zdHJ1Y3Rvcihcblx0XHRwcml2YXRlIGxvZzogTG9nZ2VyU2VydmljZVxuXHQpIHsgfVxuXHRhZGRSZXBvcnQocmVwb3J0OiBVc2VyUmVwb3J0KSB7XG5cdFx0dGhpcy5fcmF3UmVwb3J0cy5wdXNoKHJlcG9ydCk7XG5cdH1cblx0Z2V0UmF3UmVwb3J0c0FuZEZsdXNoKCkge1xuXHRcdHJldHVybiB0aGlzLl9yYXdSZXBvcnRzLnNwbGljZSgwLCB0aGlzLl9yYXdSZXBvcnRzLmxlbmd0aCk7XG5cdH1cbn1cbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBrb2EvY29yc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbS1iYWNrZW5kL2NvcmVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG0tYmFja2VuZC9jb3JlL3NyYy9saWIvYXBwbGljYXRpb25zL21haW4uYXBwXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImtvYS1qb2ktcm91dGVyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImtvYS1yYXRlbGltaXRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicGdcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVmbGVjdC1tZXRhZGF0YVwiKTsiLCIvLyBUaGUgbW9kdWxlIGNhY2hlXG52YXIgX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fID0ge307XG5cbi8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG5mdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuXHR2YXIgY2FjaGVkTW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXTtcblx0aWYgKGNhY2hlZE1vZHVsZSAhPT0gdW5kZWZpbmVkKSB7XG5cdFx0cmV0dXJuIGNhY2hlZE1vZHVsZS5leHBvcnRzO1xuXHR9XG5cdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG5cdHZhciBtb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdID0ge1xuXHRcdC8vIG5vIG1vZHVsZS5pZCBuZWVkZWRcblx0XHQvLyBubyBtb2R1bGUubG9hZGVkIG5lZWRlZFxuXHRcdGV4cG9ydHM6IHt9XG5cdH07XG5cblx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG5cdF9fd2VicGFja19tb2R1bGVzX19bbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG5cdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG5cdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbn1cblxuIiwiaW1wb3J0ICdyZWZsZWN0LW1ldGFkYXRhJztcbmltcG9ydCB7IGJvb3RzdHJhcCwgbG9hZENvbmZpZ0ZpbGUgfSBmcm9tICdAbS1iYWNrZW5kL2NvcmUnO1xuaW1wb3J0IHsgQXBwTW9kdWxlIH0gZnJvbSAnQGFwcC9hcHAubW9kdWxlJztcblxubG9hZENvbmZpZ0ZpbGUoJ2RiJyk7XG5cbmJvb3RzdHJhcChBcHBNb2R1bGUpO1xuIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9